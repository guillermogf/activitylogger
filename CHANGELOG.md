# Changelog

## 0.1.0
### Added
* README.md, LICENSE and CHANGELOG.md files.
* Log active time to SQLite database.
* debug option (prints to sdtout instead of writing to db).
* verbose option (prints to sdtout some warnings).
* help and version options.
* Option to choose frequency of `xprintidle` invocations.
* Option to choose threshold time to consider the machine idle.
* Option to change database path.
* Option to keep running even if `xprintidle` fails.
* Ability to provide time input with units (seconds, minutes and hours).
