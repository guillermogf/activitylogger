Activity Logger
===============

Activity logger is a daemon that logs the activity of the user.
Its purpose is to provide the user an easy way of keeping track on the amount
of time spent in front of their computer.

The daemon will regularly compute usage time and save to an SQLite database.

It is recommended to invoke the daemon with a cron job every time the computer
is rebooted.

The data is still only provided as raw data, i.e., it is not processed in any
way. However, a report tool is a top priority on the to-do list.


Dependencies
------------
* Python 2.7+ or Python 3+ 
* xprintidle


Usage
-----

usage: activitylogger.py [-h] [-D] [--debug] [-p] [-v] [--version] [-T TIME]
                         [-t TIME] [-f PATH]

Options:

*   -h or --help will show the help message and exit.

*   -D or --daemon will run Activity Logger as a background process.

*   --debug will print database updates instead of writing them to disk.

*   -p, --persistent will stop Activity Logger from exiting when `xprintidle`
    fails. This is recommended when run from a cron job, as the program will
    be started before the X server.

*   -v or --verbose will print warnings when needed.

*   --version will print version number and exit.

*   -T TIME or --period TIME    Input time between xprintidle invocations.
    Defaults to 10s.

*   -t TIME, --time TIME or --threshold TIME  Threshold to consider computer
    idle, i.e. minimum amount of time after last user input to consider the
    computer idle. Defaults to 60s.

*   -f PATH or --file PATH    Database location. Defaults to
    $HOME/.config/activitylogger/idle.db

`TIME` input must consist of a number followed by its units (e.g. 30s).
Allowed units are 's' for seconds, 'm' for minutes and 'h' for hours.
The number can be either an interger or a float, but it will only accept
a dot as a decimal separator.


Design
------

The easiest way of getting the usage time would be saving the current uptime
before halting. However this would not take into account, e.g., idle or sleep
time. To circumvent this problem, Activity Logger uses the `xprintidle` tool,
which is usually available on most GNU/Linux distribution's software
repository. This tool returns the amount of time that has passed since last
user interaction.

As it can be easily thought, this approach is far from being error-free. The
first problem to come into mind is that it will make Activity Logger unable
to track the time spent on a console outside the X server, as well as time
spent reading long documents or watching videos. The video part can be
partially solved by not counting as idle any time the X server is on full
screen (still on the to-do list), the reading issue by using a higher threshold
for when the previous time period is considered idle. Lastly, but not less
importantly, the activity outside the X server could be measured with some
kind of key logger. However, those kind of tools usually require root access,
so it may be well out of the scope of this piece of software and will be kept
in the to-do list until a simpler fix is found.


To-do
-----
* Activity Report tool.
* Log full screen time.
* Log activity outside the X server.


Semantic Versioning
-------------------

This projects uses [Semantic Versioning](http://semver.org/)


Keep a Changelog
----------------

This projects tries to follow [Keep a Changelog](http://keepachangelog.com/) guidance.
