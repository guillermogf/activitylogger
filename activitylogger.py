#!/usr/bin/env python3
# coding: utf-8

# 2017 © Guillermo Gómez Fonfría <guillermogf@disroot.org>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import time
import os
import sys
import subprocess
import signal
import argparse
import sqlite3


version = "0.1.0"


def main():
    parse_args()

    if show_version:
        print("Activity logger version {0}".format(version))
        sys.exit(0)

    # Daemonize
    if daemon:
        pid = os.fork()
        if pid != 0:  # Exit if parent process
            sys.exit(0)

    database_open(db_path)

    signal.signal(signal.SIGTERM, exit)

    # start_time stores the time when the program is first run
    # During execution it will only be updated when the computer
    # stops being idle with the new time
    # It is the value writen to the first column of the database
    global start_time
    start_time = time.time()

    last_idle_time = float('inf')

    while True:
        idle_time = get_idletime()
        if idle_time >= threshold and idle_time < last_idle_time:
            database_add(start_time, time.time())
            last_idle_time = idle_time
        elif idle_time >= threshold:
            # Keep updating start_time until computer stops being idle
            start_time = time.time()
            last_idle_time = idle_time
        time.sleep(sleep_time)


def unit_parser(time_string):
    """Parse time string and return value in seconds. Supported units
    are seconds (s), minutes (m) and hours (h). Default unit is
    seconds.

    If an unrecognized unit or format is used, ValueError will be
    raised.
    """

    # If value can be directly converted to float type, assume seconds
    try:
        seconds = float(time_string)
        return float(seconds)
    except ValueError:
        pass
    unit = time_string[-1]
    unit = unit.lower()
    value = time_string[0:-1]

    # Will raise ValueError if an unrecognized format is used
    seconds = float(value)

    if unit == "s":
        seconds = value
    elif unit == "m":
        seconds = value * 60
    elif unit == "h":
        seconds = value * 3600
    else:
        raise ValueError("Unknown unit")

    # It's already a float, but fails otherwise o.O
    return float(seconds)


def parse_args():
    """Command line arguments parser"""

    description = "Activity logger logs user's activity to help keeping " \
                  "track on the amount of time spent in front of their " \
                  "computer."
    epilog = "All time inputs can be followed by the corresponding unit " \
             "(i.e 's' for seconds, 'm' for minutes or 'h' for hours). " \
             "If missing, seconds will be considered the default."
    daemon_help = "Run as a background process."
    debug_help = "Print database updates instead of writing them to disk."
    persistent_help = "Keep running even if `xprintidle` fails. It will " \
                      "print a warning if used with --verbose. This is " \
                      "recommended when run from a cron job, as the " \
                      "program will be started before the X server."
    verbose_help = "Print warnings when needed."
    version_help = "Show version number and exit."
    period_help = "Time between xprintidle invocations. Defaults to 10s"
    threshold_help = "Threshold to consider computer idle, i.e. minimum " \
                     "amount of time after last user input to consider the " \
                     "computer idle. Defaults to 60s"
    file_help = "Database location. Defaults to $HOME/.config/" \
                "activitylogger/idle.db"

    db_default_path = os.path.expanduser("~/.config/activitylogger/idle.db")

    parser = argparse.ArgumentParser(description=description, epilog=epilog)
    parser.add_argument("-D", "--daemon", help=daemon_help,
                        action="store_true")
    parser.add_argument("--debug", help=debug_help, action="store_true")
    parser.add_argument("-p", "--persistent", help=persistent_help,
                        action="store_true")
    parser.add_argument("-v", "--verbose", help=verbose_help,
                        action="store_true")
    parser.add_argument("--version", help=version_help, dest="show_version",
                        action="store_true")
    parser.add_argument("-T", "--period", help=period_help, dest="sleep_time",
                        default="10s", metavar="TIME")
    parser.add_argument("-t", "--time", "--threshold", help=threshold_help,
                        dest="threshold", default="60s", metavar="TIME")
    parser.add_argument("-f", "--file", help=file_help, dest="db_path",
                        default=db_default_path, metavar="PATH")

    args = parser.parse_args()

    global daemon
    global debug
    global persistent
    global verbose
    global show_version
    global sleep_time
    global threshold
    global db_path

    daemon = args.daemon
    debug = args.debug
    persistent = args.persistent
    verbose = args.verbose
    show_version = args.show_version
    db_path = args.db_path

    # Get values in seconds and output error message if bad syntax is used
    try:
        sleep_time = unit_parser(args.sleep_time)
        threshold = unit_parser(args.threshold)
    except ValueError:
        parser.error("Time inputs must consist of a number followed by"
                     "its units ('s' for seconds, 'm' for minutes, 'h' for"
                     "hours) with no spaces between them (e.g. '30s').\n\n"
                     "The number can be a float (with a dot as decimal"
                     "separator) or a interger.")


def get_idletime():
    """Runs `xprintidle` and returns idle time"""
    try:
        idle_time = subprocess.getoutput("xprintidle")
        idle_time = float(idle_time)/1000  # Convert ms -> s
    except:
        # If `xprintidle` invocation fails or returns a non numeric value
        # exit if --persistent is not present
        if not persistent:
            print("Error running `xprintidle`", file=sys.stderr)
            sys.exit(1)
        if verbose:
            print("WARNING: Error running `xprintidle`")
        # Consider the computer active
        idle_time = 0
    return idle_time


def database_open(path):
    """Checks if database and path exist and creates them if not """
    folder = os.path.dirname(path)
    if not os.path.isdir(folder):
        os.mkdir(folder)

    global db_conn
    global db_cursor
    db_conn = sqlite3.connect(path)
    db_cursor = db_conn.cursor()
    db_cursor.execute("create table if not exists activity "
                      "(start decimal(10,1), end decimal(10,1))")
    db_conn.commit()


def database_add(start_value, end_value):
    if debug:
        print("({0}, {1})".format(start_value, end_value))
    else:
        db_cursor.execute("insert into activity values ({0},{1})"
                          .format(start_value, end_value))
        db_conn.commit()


def exit(signum, isf):
    """Clean up when TERM signal is detected."""
    if verbose:
        print("TERM signal cought, exiting...")
    database_add(start_time, time.time())
    db_conn.close()
    sys.exit(0)


main()
